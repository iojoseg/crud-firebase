package mx.tecnm.misantla.ejemplofirebase

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import mx.tecnm.misantla.ejemplofirebase.databinding.ActivityUpdateDataBinding

class UpdateData : AppCompatActivity() {

    private lateinit var binding: ActivityUpdateDataBinding
    private lateinit var database: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUpdateDataBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnActualizar.setOnClickListener {

            val nombre = binding.EdtNombre.text.toString()
            val apellido = binding.EdtApellidoP.text.toString()
            val edad = binding.EdtEdad.text.toString()

            updateData(nombre,apellido,edad)
        }
    }

    private fun updateData(nombre: String, apellido: String, edad: String) {

        database = FirebaseDatabase.getInstance().getReference("Usuarios")

        val user = mapOf<String,String>(
            "nombre" to nombre,
            "apellidoP" to apellido,
            "edad" to edad
        )

        database.child(nombre).updateChildren(user).addOnCompleteListener {
            binding.EdtNombre.text.clear()
            binding.EdtApellidoP.text.clear()
            binding.EdtEdad.text.clear()

            Toast.makeText(this, "Se actualizo correctamente", Toast.LENGTH_SHORT).show()
        }.addOnFailureListener {
            Toast.makeText(this, "No se actualizo ", Toast.LENGTH_SHORT).show()
        }
    }
}