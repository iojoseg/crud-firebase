package mx.tecnm.misantla.ejemplofirebase

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import mx.tecnm.misantla.ejemplofirebase.databinding.ActivityDeleteDataBinding
import mx.tecnm.misantla.ejemplofirebase.databinding.ActivityReadDataBinding

class DeleteData : AppCompatActivity() {
    private lateinit var binding : ActivityDeleteDataBinding
    private  var reference : DatabaseReference? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDeleteDataBinding.inflate(layoutInflater)
        setContentView(binding.root)


        binding.btnEliminar.setOnClickListener {
            val username : String = binding.EdtEliminar.getText().toString()

            if(!username.isEmpty()){
                deleteData(username)
            }else{
                Toast.makeText(this, "Ingrese un username", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun deleteData(username: String) {
        reference = FirebaseDatabase.getInstance().getReference("Usuarios")
        reference!!.child(username).removeValue().addOnCompleteListener {
            if(it.isSuccessful){
                Toast.makeText(this,"Se borro correctamente", Toast.LENGTH_SHORT).show()
                binding.EdtEliminar.text.clear()
            }else{
                Toast.makeText(this,"Ocurrio un fallo", Toast.LENGTH_SHORT).show()
            }
        }

    }
}