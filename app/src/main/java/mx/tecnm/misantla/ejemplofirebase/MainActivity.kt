package mx.tecnm.misantla.ejemplofirebase

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import mx.tecnm.misantla.ejemplofirebase.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var database : DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        //database.setValue("Hola Misantla")
        binding.btnIngresar.setOnClickListener {
            val nombre = binding.edtNombre.text.toString()
            val apellido = binding.edtApellido.text.toString()
            val edad = binding.edtEdad.text.toString()

            database = FirebaseDatabase.getInstance().getReference("Usuarios")
            val usuario = User(nombre,apellido,edad)
            database.child(nombre).setValue(usuario).addOnSuccessListener {

                binding.edtNombre.text.clear()
                binding.edtApellido.text.clear()
                binding.edtEdad.text.clear()

                Toast.makeText(this,"Se guardo correctamente", Toast.LENGTH_SHORT).show()
            }.addOnFailureListener {
                Toast.makeText(this,"No se guardo la informacion", Toast.LENGTH_SHORT).show()
            }



        }

        binding.btnLeerDatos.setOnClickListener {
            val intent = Intent(this, ReadData::class.java)
            startActivity(intent)
        }

        binding.btnEliminar.setOnClickListener {
            val intent = Intent(this,DeleteData::class.java)
            startActivity(intent)
        }

        binding.btnActualizar.setOnClickListener {
            val intent = Intent(this,UpdateData::class.java)
            startActivity(intent)
        }
    }
}